#include <SFML/Graphics.hpp>
#include <iostream>


using namespace sf;
using namespace std;
#include "sysFunctions.h"
#include "game.h"

int main() {
	
	srand(unsigned(time(NULL)));
	Game game;
	while (game.m_working) {
		game.update();
	}
	
	return 0;
}