#pragma once
#include "map.h"
#include "Objects.h"
#include "camera.h"
#include "Characters.h"

class Game {
private:
	sf::Clock m_clock;
	float m_delta;
public:
	bool m_working = true;
	RenderWindow * m_window;
	Map * m_map;
	Camera * m_camera;
	Event m_events;
	Characters * m_characters;
	
	Game();
	Game::~Game();
	void update();
	void step();
	void events();
	void draw();
};