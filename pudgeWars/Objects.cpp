#include "Objects.h"


//Pudge clas
Pudge::Pudge(sf::Vector2f position) {
	m_position = position;
	m_hook = new Hook(this);
	m_shape.setFillColor(sf::Color::Red);
	m_shape.setPosition(position);
	m_shape.setRadius(50);
	m_shape.setOrigin(sf::Vector2f(m_shape.getRadius(), m_shape.getRadius()));
	m_targetPoint = m_shape.getPosition();

	
	healthLine.setFillColor(sf::Color::Green);
	healthLine.setSize(sf::Vector2f(float(health), 20.f));
	healthLine.setOrigin(sf::Vector2f(50.f, 20 * 0.5f));
	redLine.setFillColor(sf::Color::Red);
	redLine.setSize(sf::Vector2f(100.f, 20.f));
	redLine.setOrigin(sf::Vector2f(health * 0.5f, 20 * 0.5f));
}

Pudge::~Pudge() {
	delete(m_hook);
}
unsigned Pudge::getLevel() {
	return level;
}

void Pudge::levelUP() {
	level++;
}

void Pudge::draw(sf::RenderWindow & window) {
	if (!m_dead) {
		healthLine.setPosition(m_shape.getPosition().x, m_shape.getPosition().y - m_shape.getRadius() - 20);
		redLine.setPosition(m_shape.getPosition().x, m_shape.getPosition().y - m_shape.getRadius() - 20);
		window.draw(redLine);
		window.draw(healthLine);
		m_hook->draw(window);
		window.draw(m_shape);
	}
}

sf::Vector2f Pudge::getPosition() {
	return m_position;
}


void Pudge::move(float delta) {
	if (!m_hooked && getLenght(m_shape.getPosition(), m_targetPoint) > m_shape.getRadius() * 0.1) {
		Vector2f direction = normalize(Vector2f(m_targetPoint) - m_shape.getPosition());
		m_shape.move(delta * m_speed * direction);
	}
	if (m_hook->working)
		m_hook->process(delta);
	m_position = m_shape.getPosition();

	if (m_hooked) {
		m_shape.setFillColor(sf::Color::Cyan);
	}
}

float Pudge::getSize() {
	return m_shape.getRadius();
}

void Pudge::newTarget(sf::Vector2f newPoint) {
	m_targetPoint = newPoint;
}

void Pudge::bump(unsigned damage) {
	if (damage >= health) {
		m_dead = true;
		health = 0;
	}
	else {
		health -= damage;
	}
	healthLine.setSize(sf::Vector2f(float(health), 20.f));
}
//Enemy class

void Enemy::move(float delta) {
	if (!m_hooked) {
		if (getLenght(m_shape.getPosition(), m_targetPoint) > m_shape.getRadius() * 0.1) {
			if (m_delay < delta) {
				delta -= m_delay;
				m_delay = 0;
			}
			else {
				m_delay -= delta;
			}
			if (m_delay <= 0) {
				Vector2f direction = normalize(Vector2f(m_targetPoint) - m_shape.getPosition());
				m_shape.move(delta * m_speed * direction);
			}
		}
		else {
			float num1, num2;
			num1 = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / 3200));
			num2 = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / 3200));
			m_delay = 1000;//static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / 100));
			newTarget(sf::Vector2f(num1, num2));
		}
	}
	if (m_hook->working)
		m_hook->process(delta);
	m_position = m_shape.getPosition();

	if (m_hooked) {
		m_shape.setPosition(m_enemyHook->getTipPosition());
		m_shape.setFillColor(sf::Color::Cyan);
	}
}
//Player class
/*
Player::Player() : Pudge() {
	std::cout << "player init";
}
*/


void Player::setTarget(sf::Vector2f targetPoint) {
	m_targetPoint = Vector2f(Vector2i(targetPoint));
}

//HOOK class

Hook::Hook(Pudge * host): m_host(host) {
	//line.setFillColor(Color(0, 0, 255));
	//line.setOrigin(0, lineHeight * 0.5f);
	tip.setRadius(4);
	tip.setOrigin(tip.getGlobalBounds().width * 0.5f, tip.getGlobalBounds().height * 0.5f);
	tip.setFillColor(Color(0, 0, 0));
}

void Hook::start(Vector2f cursor) {
	if (!working) { //���� ��� �� �������
		m_realLenght = 0;
		increase = true;
		m_succes = false;
		m_free = true;
		working = true;
		Vector2f buffer = cursor - m_host->getPosition();
		m_firstPositionOfHost = m_host->getPosition();
		m_firstAngle = float(atan2(buffer.y, buffer.x) * 180.f / M_PI);
		tip.setPosition(m_host->getPosition());
	}
}

void Hook::draw(sf::RenderWindow & window) {
	if (working) {
		//window.draw(line);
		window.draw(tip);
	}
}

void Hook::process(float delta){
	if (working) {
		if (increase && !m_succes) {//���� ��� �������������
			if (m_maxLenght > m_realLenght) {
				m_realLenght += speed * delta;
			}
			else {
				increase = false;
				m_realLenght = float(getLenght(tip.getPosition(), m_host->getPosition()));
			}
		}
		else {//���� �����������
			if (m_realLenght > 0)
				m_realLenght -= speed * delta;
			else {//���� ��� ���������� �� 0, �� ��������� ���
				m_realLenght = 0;
				increase = true;
				working = false;
				if (m_succes) {
					m_prey->newTarget(getTipPosition());
					m_prey->m_hooked = false;
				}
				}
			}
		/*
		line.clear();
		sf::VertexArray line(sf::LinesStrip, 0);

		// Calculate the points on the curve (10 segments)
		std::vector<sf::Vector2f> points =
			CalcCubicBezier(
				sf::Vector2f(0, 100),
				sf::Vector2f(300, 200),
				sf::Vector2f(0, 150),
				sf::Vector2f(300, 150),
				10);

		// Append the points as vertices to the vertex array
		for (std::vector<sf::Vector2f>::const_iterator a = points.begin(); a != points.end(); ++a)
			line.append(sf::Vertex(*a, sf::Color::Red));

		*/
		//line.setSize(Vector2f(m_realLenght, lineHeight));
		Vector2f vec;
		float angle;
		if (increase) {
			angle = m_firstAngle;
		}
		else {
			vec = tip.getPosition() - m_host->getPosition();
			angle = getDirectionAngle(vec);
		}
		//line.setPosition(Vector2f(m_host->getPosition().x, m_host->getPosition().y));
		//line.setRotation(angle);

		Vector2f buffer = polarToCartesianSys(m_realLenght, angle);
		if (increase) 
			buffer = Vector2f(buffer.x + m_firstPositionOfHost.x, buffer.y + m_firstPositionOfHost.y);
		else
			buffer = Vector2f(buffer.x + m_host->getPosition().x, buffer.y + m_host->getPosition().y);
		tip.setPosition(buffer);
	}
}

sf::Vector2f Hook::getTipPosition() {
	return  tip.getPosition();
}

float Hook::getTipSize() {
	return  tip.getRadius();
}

void Hook::pickedUp(Pudge * enemy) {
	enemy->m_hooked = true;
	enemy->m_enemyHook = this;
	enemy->bump(damage);
	m_prey = enemy;
	m_succes = true;
	m_free = false;
}