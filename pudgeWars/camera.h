#pragma once
#include "loadlib.h"

class Camera {
private:
	sf::View * m_view;
	float m_padding = 50;
	float m_speed = 1;
	sf::Vector2f m_mapSize;
public:
	Camera(sf::RenderWindow *, sf::Vector2f, sf::Vector2f);
	~Camera();

	void update(sf::RenderWindow *, float);

	void moveToWard(sf::Vector2f);
	
	void moveToAxes(unsigned int, float);
};