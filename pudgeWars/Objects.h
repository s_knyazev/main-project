#pragma once
#include "loadlib.h"

class Pudge;
class Player;
class Hook;

class Pudge {
protected:
	unsigned level = 1;
	float m_speed = 0.4f;
	unsigned health = 100;
	sf::CircleShape m_shape;
	sf::Vector2f m_targetPoint;
	sf::RectangleShape healthLine;
	sf::RectangleShape redLine;
public:
	bool m_dead = false;
	bool m_hooked = false;
	sf::Vector2f m_position = sf::Vector2f(0, 0);
	sf::Vector2f getPosition();
	Hook * m_enemyHook;
	Hook * m_hook = nullptr;
	Pudge(sf::Vector2f);
	~Pudge();
	unsigned getLevel();
	float getSize();
	void levelUP();
	void draw(sf::RenderWindow & window);
	void move(float delta);
	void newTarget(sf::Vector2f);
	void bump(unsigned);
};
	
class Enemy :public Pudge {
	float m_delay = 0;
public:

	Enemy(sf::Vector2f position) : Pudge(position)
	{};
	void move(float delta);
};

class Player :public Pudge {
public:
	
	Player(sf::Vector2f position) : Pudge(position)
	{
		m_shape.setFillColor(sf::Color::Blue);
	};
	
	void setTarget(sf::Vector2f targetPoint);
};


class Hook {
private:
	float speed = 1;
	float m_maxLenght = 500;
	bool increase;
	float m_realLenght = 0;
	float lineHeight = 5;
	Pudge * m_host;
	//������
	//RectangleShape line;
	sf::VertexArray line;
	sf::CircleShape tip;
	sf::Vector2f m_firstPositionOfHost;//����������� ������������ ���������
	float m_firstAngle;//����������� ���� �������
	Pudge * m_prey;
	bool m_succes = false;
public:	
	unsigned damage = 25;
	bool m_free = true;
	bool working = false;


	Hook(Pudge * host);
	void start(Vector2f cursor);
	void process(float delta);
	void draw(sf::RenderWindow & window);
	sf::Vector2f getTipPosition();
	float getTipSize();
	void pickedUp(Pudge *);
	/*
	void pickedUp(Enemy *);
	void pickedUp(Player *);
	*/
};
