#include "map.h"

Border::Border(sf::Vector2f mapSize, unsigned int positionOnSide, unsigned int depth) {

	switch (positionOnSide) {
	case 0://top position
		m_position = sf::Vector2f(mapSize.x * 0.5f, 0);
		m_size.x = mapSize.x;
		m_size.y = float(depth);
		break;
	case 1://right position
		m_position = sf::Vector2f(mapSize.x, mapSize.y * 0.5f);
		m_size.x = float(depth);
		m_size.y = mapSize.x;
		break;
	case 2://bottom position
		m_position = sf::Vector2f(mapSize.x * 0.5f, mapSize.y);
		m_size.x = mapSize.x;
		m_size.y = float(depth);
		break;
	case 3://left position
		m_position = sf::Vector2f(0, mapSize.y * 0.5f);
		m_size.x = float(depth);
		m_size.y = mapSize.y;
		break;
	}
	m_shape.setSize(m_size);
	m_shape.setOrigin(m_size.x * 0.5, m_size.y * 0.5);
	m_shape.setPosition(m_position);
	m_shape.setFillColor(sf::Color::Red);
}

void Border::draw(sf::RenderWindow & window) {
	window.draw(m_shape);
}

River::River(sf::Vector2f mapSize) {

	m_texture.loadFromFile("./img/Texture/water/water.jpg");
	m_texture.setRepeated(true);
	m_shape.setPointCount(12);

	// define the points
	m_shape.setPoint(0, sf::Vector2f(mapSize.x * 0.4f, 0));
	m_shape.setPoint(1, sf::Vector2f(mapSize.x * 0.4f, mapSize.y * 0.45f));
	m_shape.setPoint(2, sf::Vector2f(mapSize.x * 0.38f, mapSize.y * 0.46f));
	m_shape.setPoint(3, sf::Vector2f(mapSize.x * 0.38f, mapSize.y * 0.54f));
	m_shape.setPoint(4, sf::Vector2f(mapSize.x * 0.4f, mapSize.y * 0.55f));
	m_shape.setPoint(5, sf::Vector2f(mapSize.x * 0.4f, mapSize.y));

	m_shape.setPoint(6, sf::Vector2f(mapSize.x * 0.6f, mapSize.y));
	m_shape.setPoint(7, sf::Vector2f(mapSize.x * 0.6f, mapSize.y * 0.55f));
	m_shape.setPoint(8, sf::Vector2f(mapSize.x * 0.62f, mapSize.y * 0.54f));
	m_shape.setPoint(9, sf::Vector2f(mapSize.x * 0.62f, mapSize.y * 0.46f));
	m_shape.setPoint(10, sf::Vector2f(mapSize.x * 0.6f, mapSize.y * 0.45f));
	m_shape.setPoint(11, sf::Vector2f(mapSize.x * 0.6f, 0.f));
	m_shape.setFillColor(sf::Color::Blue);
	m_shape.setPosition(0, 0);
	m_shape.setTexture(&m_texture);
	//shape.setTextureRect(sf::IntRect(0, 0, 20, 20));
}

void River::draw(sf::RenderWindow & window) {
	window.draw(m_shape);
}


//map class
Map::Map(sf::Vector2f mapSize) {
	m_size = mapSize;
	m_border1 = new Border(mapSize, 0, 20);
	m_border2 = new Border(mapSize, 1, 20);
	m_border3 = new Border(mapSize, 2, 20);
	m_border4 = new Border(mapSize, 3, 20);
	m_river = new River(mapSize);


	m_backgroundTexture->loadFromFile("./img/Texture/Ashen_grass/grass.jpg");//Ashen_Grass.tga", sf::IntRect(0, 0, 64, 64
	m_backgroundTexture->setRepeated(true);
	m_backgroundTexture->setSmooth(true);


	m_background.setTexture(*m_backgroundTexture);
	m_background.setTextureRect({ 0, 0, int(mapSize.x), int(mapSize.y) });
	m_background.setPosition(0, 0);

}
void Map::draw(sf::RenderWindow & window) {
	window.draw(m_background);
	m_border1->draw(window);
	m_border2->draw(window);
	m_border3->draw(window);
	m_border4->draw(window);
	m_river->draw(window);
}

sf::Vector2f Map::getCenter() {
	return sf::Vector2f(m_size.x * 0.5f, m_size.y * 0.5f);
}
sf::Vector2f Map::getSize() {
	return m_size;
}

