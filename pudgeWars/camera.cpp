#include "camera.h"

Camera::Camera(sf::RenderWindow * window, sf::Vector2f mapSize, sf::Vector2f center) {
	m_view = new sf::View(center, sf::Vector2f(float(window->getSize().x), float(window->getSize().y)));
	m_view->zoom(1.5f);//for test 3.f);
	m_mapSize = mapSize;
	window->setView(*m_view);
}

Camera::~Camera() {
	delete m_view;
}

void Camera::update(sf::RenderWindow * window, float delta) {
	sf::Vector2i cursor = sf::Mouse::getPosition(*window);
	if (cursor.x > window->getSize().x - m_padding) {
		moveToAxes(0, delta);
	}
	else if (cursor.x < m_padding) {
		moveToAxes(1, delta);
	}
	if (cursor.y > window->getSize().y - m_padding) {
		moveToAxes(2, delta);
	}
	else if (cursor.y < m_padding) {
		moveToAxes(3, delta);
	}
	window->setView(*m_view);
}

void Camera::moveToWard(sf::Vector2f center) {
	m_view->setCenter(center);
}

void Camera::moveToAxes(unsigned int diraction, float delta) {
	float shift;
	switch (diraction) {
	case(0) ://right
		if (m_mapSize.x < m_speed * delta + m_view->getCenter().x + m_view->getSize().x * 0.5) {
			shift = m_mapSize.x - (m_view->getCenter().x + m_view->getSize().x * 0.5);
		}
		else
			shift = m_speed * delta;
		m_view->move(shift, 0);
		break;
	case(1) ://left
		if (0 > -m_speed * delta + m_view->getCenter().x - m_view->getSize().x * 0.5)
			m_view->setCenter(m_view->getSize().x * 0.5, m_view->getCenter().y);
		else {
			shift = -m_speed * delta;
			m_view->move(shift, 0);
		}
		break;
	case(2) ://bottom
		if (m_mapSize.y < m_speed * delta + m_view->getCenter().y + m_view->getSize().y * 0.5) 
			m_view->setCenter(m_view->getCenter().x, m_mapSize.y - m_view->getSize().y * 0.5);
		else {
			shift = m_speed * delta;
			m_view->move(0, shift);
		}
		break;
	case(3) ://top
		if (0 > -m_speed * delta + m_view->getCenter().y - m_view->getSize().y * 0.5)
			m_view->setCenter(m_view->getCenter().x, m_view->getSize().y * 0.5);
		else {
			shift = -m_speed * delta;
			m_view->move(0, shift);
		}
		break;
	}
}