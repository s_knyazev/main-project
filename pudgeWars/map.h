#pragma once
#include "loadlib.h"

class Border {
private:
	sf::RectangleShape m_shape;
	sf::Vector2f m_position;
	sf::Vector2f m_size;
public:
	Border(sf::Vector2f mapSize, unsigned int positionOnSide, unsigned int depth);
	void draw(sf::RenderWindow & window);
};

class River {
private:
	sf::ConvexShape m_shape;
	sf::Texture m_texture;
public:
	River(sf::Vector2f m_mapSize);
	void draw(sf::RenderWindow & window);
};

class Map {
private:
	sf::Vector2f m_size;
	Border *m_border1;//top
	Border *m_border2;//right
	Border *m_border3;//bottom
	Border *m_border4;//left
	River *m_river;
	sf::Sprite m_background;
	sf::Texture * m_backgroundTexture = new sf::Texture;
public:
	Map(sf::Vector2f mapSize);
	void draw(sf::RenderWindow & window);
	sf::Vector2f getCenter();
	sf::Vector2f getSize();
};
