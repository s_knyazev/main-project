#include "—haracters.h"

Characters::Characters()
{	
	int randNum = rand() % count - 2;
	randNum = 1;
	unsigned int k = 0;
	sf::Vector2f position;
	Enemy * buffer;
	for (unsigned int i = 1; i < count + 1; i++) {
		if (i <= (count + 1) * 0.5)
			position = sf::Vector2f(300.f, 800.f * i);
		else {
			k++;
			position = sf::Vector2f(2900.f, 800.f * k);
		}
		if (randNum == i) {
			m_player = new Player(position);
		}
		else{
			buffer = new Enemy(position);
			List->push_back(buffer);
		}
		
	}
}

Characters::~Characters()
{
	for (auto it : *List) {
		delete it;
	}
	delete List;
	delete m_player;
}


void Characters::draw(sf::RenderWindow & window) {
	m_player->draw(window);
	for (auto it : *List) {
		it->draw(window);
	}
}

void Characters::move(float delta) {
	m_player->move(delta);
	for (auto it : *List) {
		it->move(delta);
	}
}

void Characters::update(float delta) {
	collisions();
	move(delta);
	for (auto it : *List) {
		it->move(delta);
	}
}

void Characters::collisions() {
	for (auto it1 : *List) {
		for (auto it2 : *List) {
			if (it1 != it2 && it1->m_hook->working && !it2->m_dead && it1->m_hook->m_free) {
				if (getLenght(it1->m_hook->getTipPosition(), it2->getPosition()) < (it1->m_hook->getTipSize() + it2->getSize())) {
					it1->m_hook->pickedUp(it2);
				}
			}
		}
	}
	if (!m_player->m_dead){
		for (auto it2 : *List) {
			if (m_player->m_hook->working && !it2->m_dead && m_player->m_hook->m_free) {
				if (getLenght(m_player->m_hook->getTipPosition(), it2->getPosition()) < (m_player->m_hook->getTipSize() + it2->getSize())) {
					m_player->m_hook->pickedUp(it2);
				}
			}
		}
	}
}

