#include "game.h"

Game::Game() {
	sf::ContextSettings settings;
	settings.antialiasingLevel = 4;
	sf::VideoMode DesktopMode = sf::VideoMode::getDesktopMode();
	m_window = new sf::RenderWindow(sf::VideoMode(1200, 600), "Pudge wars", sf::Style::Default, settings);
	//m_window = new sf::RenderWindow(DesktopMode, "Pudge wars", sf::Style::Fullscreen, settings);
	m_window->setFramerateLimit(60);
	m_window->setVerticalSyncEnabled(true);

	m_characters = new Characters;
	m_map = new Map(sf::Vector2f(3200, 3200));
	m_camera = new Camera(m_window, m_map->getSize(), m_characters->m_player->getPosition());
}

Game::~Game() {
	delete m_characters;
	delete m_map;
	delete m_camera;
}

void Game::update() {
	step();
	events();
	draw();
}

void Game::step() {
	m_delta = float(m_clock.getElapsedTime().asMicroseconds()) / 1600;
	m_clock.restart();
	m_characters->update(m_delta);
	m_camera->update(m_window, m_delta);
}

void Game::events() {
	sf::Vector2i pixelPos = sf::Mouse::getPosition(*m_window);
	sf::Vector2f cursor = m_window->mapPixelToCoords(pixelPos);
	while (m_window->pollEvent(m_events)) {
		if (m_events.type == sf::Event::MouseButtonReleased) {
			if (m_events.mouseButton.button == sf::Mouse::Right) {
				m_characters->m_player->setTarget(cursor);
			}
			if (m_events.mouseButton.button == sf::Mouse::Left) {
				m_characters->m_player->m_hook->start(cursor);
			}
		}
		if (m_events.type == sf::Event::Closed || (m_events.type == Event::KeyPressed && Keyboard::isKeyPressed(Keyboard::Escape))) {
			m_working = false;
			m_window->close();
		}
	}
}

void Game::draw() {
	m_window->clear();
	m_map->draw(*m_window);
	m_characters->draw(*m_window);
	m_window->display();
}