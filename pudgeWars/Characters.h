#pragma once
#include "loadlib.h"
#include "Objects.h"

class Characters
{
	unsigned int count = 6;
public:
	list<Enemy *> List;
	Player * m_player;
	Characters();
	~Characters();
	void draw(sf::RenderWindow & window);
	void move(float);
	void update(float);
	void collisions();
};

