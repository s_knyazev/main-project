#pragma once
#include "loadlib.h"
#include <cmath>

sf::Vector2f normalize(const sf::Vector2f& source);
sf::Vector2f polarToCartesianSys(float radius, float alpha);
double getLenght(Vector2f first, Vector2f second);
std::vector<sf::Vector2f> CalcCubicBezier(
	const sf::Vector2f &start,
	const sf::Vector2f &end,
	const sf::Vector2f &startControl,
	const sf::Vector2f &endControl,
	const size_t numSegments);

float getDirectionAngle(Vector2f vec);