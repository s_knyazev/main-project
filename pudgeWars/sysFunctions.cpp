#include "sysFunctions.h"

sf::Vector2f normalize(const sf::Vector2f& source)
{
	float length = sqrt((source.x * source.x) + (source.y * source.y));
	if (length != 0)
		return sf::Vector2f(source.x / length, source.y / length);
	else
		return source;
}

sf::Vector2f polarToCartesianSys(float radius, float alpha) {
	sf::Vector2f result;
	alpha = float(alpha * M_PI / 180);
	result.x = radius * cos(alpha);
	result.y = radius * sin(alpha);
	return result;
}

double getLenght(Vector2f first, Vector2f second) {
	double lenght = sqrt((second.x - first.x) * (second.x - first.x) + (second.y - first.y) * (second.y - first.y));//pow((enemy.x - cursor.x),2)
	return lenght;
}

std::vector<sf::Vector2f> CalcCubicBezier(
	const sf::Vector2f &start,
	const sf::Vector2f &end,
	const sf::Vector2f &startControl,
	const sf::Vector2f &endControl,
	const size_t numSegments)
{
	std::vector<sf::Vector2f> ret;
	if (!numSegments) // Any points at all?
		return ret;

	ret.push_back(start); // First point is fixed
	float p = 1.f / numSegments;
	float q = p;
	for (size_t i = 1; i < numSegments; i++, p += q) // Generate all between
		ret.push_back(p * p * p * (end + 3.f * (startControl - endControl) - start) +
			3.f * p * p * (start - 2.f * startControl + endControl) +
			3.f * p * (startControl - start) + start);
	ret.push_back(end); // Last point is fixed
	return ret;
}


float getDirectionAngle(Vector2f vec) {
	return float(atan2(vec.y, vec.x) * 180.f / M_PI);
}